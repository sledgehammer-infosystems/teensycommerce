# frozen_string_literal: true

require_relative "lib/teensy/catalog/version"

Gem::Specification.new do |spec|
  spec.name = "teensy-catalog"
  spec.version = Teensy::Catalog::VERSION
  spec.authors = ["Brad Thompson"]
  spec.email = ["bkt@brad-thompson.com"]

  spec.summary = "A tiny e-commerce catalog"
  # spec.description = "TODO: Write a longer description or delete this line."
  spec.homepage = "https://teensycommerce.com"
  spec.required_ruby_version = ">= 3.2.0"

  spec.metadata["allowed_push_host"] = "TODO: Set to your gem server 'https://example.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/TODO"
  spec.metadata["changelog_uri"] = "https://gitlab.com/TODO"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) || f.start_with?(*%w[bin/ test/ spec/ features/ .git .circleci appveyor])
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "sinatra", "~> 3.0"
  spec.add_dependency "sinatra-contrib", "~> 3.0"
  spec.add_dependency "rack-contrib", "~> 2.3"
  spec.add_dependency "money", "~> 6.16"
  spec.add_dependency "activesupport", "~> 7.0"
  spec.add_dependency "stripe", "~> 9.0"
  spec.add_dependency "dotenv", "~> 2.8"

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
