module Teensy
  module Checkouts
    class Stripe < Checkout
      def initialize
        super
        ::Stripe.api_key = ENV['STRIPE_KEY']
      end

      def button
        "Check Out with Stripe"
      end
    end
  end
end