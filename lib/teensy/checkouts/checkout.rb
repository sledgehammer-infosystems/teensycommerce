module Teensy
  module Checkouts
    class Engine
      cattr_reader :checkouts

      def self.register(checkout)
        @@checkouts ||= {}
        @@checkouts[checkout.slug] = checkout
      end

      def self.[](key)
        @@checkouts[key]
      end

      def self.each
        @@checkouts.each do |c|
          yield c
        end
      end
    end

    class Checkout
      def initialize
        Engine.register(self)
      end

      def slug
        self.class.name.split("::").last.downcase
      end

      def view
        self.class.name.split("::").last.downcase
      end

      def button
        "Check out with #{slug}"
      end
    end
  end
end