module Teensy
  module Checkouts
    class Quote < Checkout
      def button
        "Print an Invoice"
      end
    end
  end
end