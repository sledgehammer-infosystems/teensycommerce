module Teensy
  module Cart
    class Cart
      attr_reader :items, :pricing
      attr_accessor :number
      cattr_accessor :path, :prefix
      attr_accessor :checkout
      def initialize(payload)
        @items = []
        payload.each do |i|
          @items << Teensy::Cart::Item.from_cart(i)
        end
        @pricing = Teensy::Pricing::Engine.order_price(self)
      end
      
      def empty?
        @items.empty? && @pricing.lines.empty?
      end

      def <<(item)
        if item.is_a? Teensy::Cart::Item
          @items << item
        else
          @items << Teensy::Cart::Item.new(*item)
        end
      end

      def remove(item)
        ix = @items.index(Item.from_cart(item))
        @items.delete_at(ix)
      end

      def to_json
        @items.map(&:as_json).to_json
      end

      def to_stripe
        {
          line_items: @pricing.lines.select{ |l| l.min_price.to_i.abs > 0 }.map(&:to_stripe),
          mode: 'payment',
          success_url: 'http://localhost:9292/payment_success',
          cancel_url: 'http://localhost:9292/',
        }
      end

      def checkout=(checkout)
        @checkout = checkout
        @pricing = Teensy::Pricing::Engine.order_price(self)
      end
    end

    class Item
      attr_reader :id, :name, :description, :fields, :data, :customizations, :line_item_descriptor, :price

      def initialize(args = {})
        @id = args[:id]
        @name = args[:name]
        @description = args[:description]
        @fields = HashWithIndifferentAccess.new(args[:fields])
        @data = HashWithIndifferentAccess.new(args[:data])
        @customizations = HashWithIndifferentAccess.new(args[:customizations]).filter! { |k, v| fields.keys.include? k }
        @line_item_descriptor = args[:line_item_descriptor] || @id
        @price = Teensy::Pricing::Engine.product_price(self)
      end

      def ==(other)
        @id = other.id && @name == other.name && @customizations == other.customizations
      end

      def complete?
        @fields.select { |k, f| f.required }.all? { |k, f| !@customizations[k]&.empty? }
      end

      def as_json(*args)
        {
          id: id,
        }.merge(customizations)
      end

      def to_json
        as_json.to_json
      end

      def as_line_item_label
        label = @line_item_descriptor
        @customizations.each do |k, v|
          label = label.gsub("%{#{k}}", v)
        end
        label
      end

      def self.from_cart(payload)
        from_product(Teensy::Catalog::Product.find(payload['id']), payload)
      end

      def self.from_product(product, customizations = {})
        new(
          id: product.id,
          name: product.name,
          description: product.description,
          fields: product.fields,
          data: product.data,
          line_item_descriptor: product.line_item_descriptor,
          customizations: customizations
        )
      end
    end
  end
end