# frozen_string_literal: true

require 'dotenv/load'
require 'sinatra'
require 'sinatra/namespace'
require 'sinatra/cookies'
require 'rack/contrib'
require 'money'
require 'stripe'
require 'active_support'
require 'active_support/core_ext/hash/indifferent_access'
require 'active_support/core_ext/hash/keys'
require 'ostruct'

require_relative "catalog/version"
require_relative "pricing/engine"
require_relative "pricing/helpers"
require_relative "cart/cart"
require_relative "catalog/app"
require_relative "checkouts/checkout"
require_relative "checkouts/quote"
require_relative "checkouts/stripe"


I18n.config.available_locales = :en
Money.locale_backend = :i18n
Money.default_currency = 'USD'
Money.rounding_mode = BigDecimal::ROUND_HALF_UP

module Teensy
  module Catalog 
    class Error < StandardError; end

    class Product
      attr_reader :id, :name, :description, :fields, :data, :line_item_descriptor
      def initialize(args = {})
        @@catalog ||= HashWithIndifferentAccess.new
        @id = args[:id]
        @name = args[:name]
        @description = args[:description]
        fields = HashWithIndifferentAccess.new(args[:fields])
        @fields = {}
        fields.each do |k, v|
          @fields[k] = CustomField.new(k, v)
        end
        @data = HashWithIndifferentAccess.new(args[:data])
        @line_item_descriptor = args[:line_item_descriptor]
        @@catalog[id] = self
      end

      def self.all
        @@catalog
      end

      def self.each 
        @@catalog.each { |p| yield p }
      end

      def self.find(id)
        @@catalog[id]
      end

      def merge(item)
        OpenStruct.new({
          
      }.with_indifferent_access.merge(item))
      end

      def price
        Teensy::Pricing::Engine.product_price(self)
      end

      def customizations 
        {}
      end

      def as_line_item_label
        @name
      end
    end

    class CustomField
      attr_reader :id, :type, :label, :required, :options, :default
      def initialize(key, payload)
        @id = key
        @type = payload[:type]
        @label = payload[:label]
        @required = payload[:required] || false
        @options = payload[:options]
        @default = payload[:default] || case @type
          when 'Numeric'
            0
          else
            ''
        end
      end
    end
  end
end
