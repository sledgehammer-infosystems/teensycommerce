module Teensy
  module Catalog 
    class App < ::Sinatra::Base
      cattr_reader :pricing_engine
      register Sinatra::Namespace
      helpers Sinatra::Cookies
      helpers Teensy::Pricing::Helpers

      configure do
        Teensy::Pricing::Engine.register(Teensy::Pricing::SimplePricingMiddleware.new)
      end

      helpers do
        def find_template(views, name, engine)
          views = [] + Array(views) unless views.is_a?(Array)
          views.unshift('./views')
    
          views.each do |path|
            yield ::File.join(path, "#{name}.#{@preferred_extension}")
    
            Tilt.default_mapping.extensions_for(engine).each do |ext|
              yield ::File.join(path, "#{name}.#{ext}") unless ext == @preferred_extension
            end
          end
        end
      end

      before do
        @cart = Teensy::Cart::Cart.new(JSON.parse(cookies[:cart] || '[]'))
      end
      
      get '/' do
        erb :index
      end

      delete '/?' do
        @cart.remove(JSON.parse(CGI.unescape(params['item'])))
        cookies[:cart] = @cart.to_json
        
        erb :index
      end

      namespace '/catalog' do
        get '/?' do
          'Catalog!'
        end

        get '/:id' do
          @product = Teensy::Cart::Item.from_product(Teensy::Catalog::Product.find(params['id'].to_sym), params)
          @pricing = Teensy::Pricing::Engine.product_price(@product) if @product
          erb :index
        end

        patch '/:id' do
          @product = Teensy::Cart::Item.from_product(Teensy::Catalog::Product.find(params['id'].to_sym), params)
          @pricing = Teensy::Pricing::Engine.product_price(@product) if @product
          erb :price_frame, layout: false
        end

        post '/:id' do
          @product = Teensy::Cart::Item.from_product(Teensy::Catalog::Product.find(params['id'].to_sym), params)
          @pricing = Teensy::Pricing::Engine.product_price(@product) if @product
          @cart << @product
          cookies[:cart] = @cart.to_json
          erb :index
        end

        delete '/:id' do
          @cart.remove(JSON.parse(CGI.unescape(params['item'])))
          @product = Teensy::Cart::Item.from_product(Teensy::Catalog::Product.find(params['id'].to_sym), params)
          @pricing = Teensy::Pricing::Engine.product_price(@product) if @product
          cookies[:cart] = @cart.to_json
          
          erb :index
        end
      end

      get '/checkouts/:method' do
        @checkout = Teensy::Checkouts::Engine[params['method']]
        @cart.checkout = @checkout
        @cart.number = Time.now.strftime('%y%m%d%H%M%S')
        if @cart.path
          File.open(File.join(@cart.path, "#{@cart.number}.json"), 'w') do |f|
            f << @cart.to_json
          end
        end
        erb "checkouts/#{@checkout.view}".to_sym
      end

      get '/invoices/?' do
        erb :invoice
      end

      post '/invoices/?' do
        path = File.join(@cart.path, "#{params['num']}.json")
        if File.exist?(path)
          @cart = Teensy::Cart::Cart.new(JSON.parse(File.read(path)))
          @cart.number = params['num']
          erb 'checkouts/quote'.to_sym
        else
          @err = 'Could not find specified invoice'
          erb :invoice
        end
      end

      namespace '/stripe' do
        before do
          @checkout = Teensy::Checkouts::Engine['stripe']
          @cart.checkout = @checkout
        end

        get '/?' do
          session = Stripe::Checkout::Session.create(@cart.to_stripe)
          redirect session.url, 303
        end
      end

      get '/payment_success' do
        cookies.delete :cart
        erb :payment_success
      end
    end
  end
end