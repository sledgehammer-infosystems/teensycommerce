# frozen_string_literal: true

module Teensy
  module Catalog
    VERSION = "0.1.0"
  end
end
