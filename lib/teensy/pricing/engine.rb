module Teensy
  module Pricing
    class Engine
      def self.register(middleware)
        @@middlewares ||= []
        @@middlewares.push middleware
      end

      def self.product_price(product)
        order_price(OpenStruct.new(items: [product]))
      end

      def self.order_price(order)
        result = PricingResult.new
        @@middlewares.each do |mw|
          mw.call(order, result)
        end

        result
      end
    end

    class PricingLine
      attr_reader :line, :min_price, :max_price, :quantity

      def initialize(line, price, quantity = 1, max_price = nil)
        @line = line
        @min_price = Money.new(price)
        @quantity = quantity.to_i
        @max_price = max_price ? Money.new(max_price) : @min_price 
      end

      def to_s
        if @min_price < @max_price
          "#{@line} #{@quantity > 1 ? "x #{@quantity} " : ''}| $#{@min_price * @quantity} - $#{@max_price * @quantity}"
        elsif @min_price == @max_price
          "#{@line} #{@quantity > 1 ? "x #{@quantity} " : ''}| $#{@min_price * @quantity}"
        else
          "#{@line} #{@quantity > 1 ? "x #{@quantity} " : ''}| Starting at $#{@min_price * @quantity}"
        end
      end


      def to_stripe
        {
          price_data: {
            currency: 'usd',
            product_data: {
              name: @line
            },
            unit_amount: @max_price.fractional
          },
          quantity: @quantity
        }
      end
    end

    class PricingResult
      attr_reader :lines, :min_total, :max_total
      def initialize
        @lines = []
        @min_total = Money.new(0)
        @max_total = Money.new(0)
      end

      def add(line, price, quantity, max_price = nil)
        PricingLine.new(line, price, quantity, max_price).tap do |pl| 
          @lines.push pl
          @min_total += pl.min_price * quantity
          @max_total += pl.max_price * quantity
        end
      end
    end

    class PricingMiddleware
      def initialize
      end

      def call(order, result)
      end
    end

    class SimplePricingMiddleware < PricingMiddleware
      def call(order, result)
        order.items.each do |p|
          if p.data['price']
            quantity = p.customizations['quantity'] && !p.customizations['quantity'].empty? ? p.customizations['quantity'].to_f : 1
            result.add(p.as_line_item_label, p.data['price'], quantity)
          end
        end
      end
    end

    class CustomPricingMiddleware < PricingMiddleware
      def initialize(&block)
        @block = block
      end

      def call(order, result)
        @block.call(order, result)
      end
    end
  end
end