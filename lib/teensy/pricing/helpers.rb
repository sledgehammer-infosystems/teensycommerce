module Teensy
  module Pricing
    module Helpers
      def price_for(pricing)
        if pricing.max_total < pricing.min_total
          "Starting at $#{pricing.min_total}"
        elsif pricing.max_total > pricing.min_total
          "$#{pricing.min_total} - $#{pricing.max_total}"
        else
          "$#{pricing.min_total}"
        end
      end
    end
  end
end